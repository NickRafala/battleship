#ifndef SHIP_H
#define SHIP_H

#include <QObject>

class Ship : public QObject
{
    Q_OBJECT
private:
    int numHoles;
    int numHits;
    bool sunk;
public:
    Ship(int size);
    ~Ship();
    void hit(); // ship was hit

    int getSize();
    void setSunk(bool _sunk);
    bool getSunk();
signals:
    void shipSunk();
};

#endif // SHIP_H
