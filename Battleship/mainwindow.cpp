#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , viewModel(new GameViewModel())
    , selectedShip(nullptr)
    , shipsPlaced(0)
    , firstRow(-1)
    , firstCol(-1)
{
    ui->setupUi(this);

    viewModel->randomizeBoard(false); // randomize opponent board

    connect(viewModel, &GameViewModel::topPinPlaced, this, [=](PinColor pin, int row, int col) {
        QLayoutItem *topLayoutItem = ui->topScreen->itemAtPosition(row, col);
        QWidget *topCell = dynamic_cast<QWidgetItem*>(topLayoutItem)->widget();
        if (pin == RED) topCell->setStyleSheet("background-color: red;");
        if (pin == WHITE) topCell->setStyleSheet("background-color: white;");
    });
    connect(viewModel, &GameViewModel::bottomPinPlaced, this, [=](PinColor pin, int row, int col) {
        QLayoutItem *bottomLayoutItem = ui->bottomScreen->itemAtPosition(row, col);
        QWidget *bottomCell = dynamic_cast<QWidgetItem*>(bottomLayoutItem)->widget();
        if (pin == RED) bottomCell->setStyleSheet("background-color: red;");
        if (pin == WHITE) bottomCell->setStyleSheet("background-color: white;");
    });
    connect(viewModel, &GameViewModel::shipSunk, this, [=](bool isPlayer, int sunk) {
        if (isPlayer)
            ui->pShipsSunk->setText("Player ships sunk: " + QString::number(sunk));
        else
            ui->oShipsSunk->setText("Opponent ships sunk: " + QString::number(sunk));

        QMessageBox *sunkDialog = new QMessageBox(QMessageBox::NoIcon, "Ship sunk", "Ship sunk");
        sunkDialog->exec();
        delete sunkDialog;
    });
    connect(viewModel, &GameViewModel::gameWin, this, [=] {
        QMessageBox *winDialog = new QMessageBox(QMessageBox::NoIcon, "You Win", "You Win");
        winDialog->exec();
        delete winDialog;
        reset();
    });
    connect(viewModel, &GameViewModel::gameLose, this, [=] {
        QMessageBox *loseDialog = new QMessageBox(QMessageBox::NoIcon, "You Lose", "You Lose");
        loseDialog->exec();
        delete loseDialog;
        reset();
    });

    // allow player to choose a place to attack
    for (int row = 0; row < DIMENSIONS; row++) {
        for (int col = 0; col < DIMENSIONS; col++) {
            QLayoutItem *topLayoutItem = ui->topScreen->itemAtPosition(row, col);
            QWidget *topCell = dynamic_cast<QWidgetItem*>(topLayoutItem)->widget();
            QPushButton *button = (QPushButton*)topCell;
            connect(button, &QAbstractButton::clicked, this, [=] { viewModel->attack(row, col); });
        }
    }

    // allow player to place a ship
    for (int row = 0; row < DIMENSIONS; row++) {
        for (int col = 0; col < DIMENSIONS; col++) {
            QLayoutItem *bottomLayoutItem = ui->bottomScreen->itemAtPosition(row, col);
            QWidget *bottomCell = dynamic_cast<QWidgetItem*>(bottomLayoutItem)->widget();
            QPushButton *button = (QPushButton*)bottomCell;
            connect(button, &QAbstractButton::clicked, this, [=] {
                if (selectedShip) { // if not null
                    bool success = false;
                    if (firstRow == -1 && firstCol == -1) {
                        firstRow = row;
                        firstCol = col;
                        return;
                    }
                    if (firstRow == row) {
                        if (firstCol < col) {
                            int shipSize = selectedShip->text().toInt();
                            success = viewModel->placeShipEast(new Ship(shipSize), firstRow, firstCol, true);
                            firstRow = -1;
                            firstCol = -1;
                        } else if (firstCol > col) {
                            int shipSize = selectedShip->text().toInt();
                            success = viewModel->placeShipWest(new Ship(shipSize), firstRow, firstCol, true);
                            firstRow = -1;
                            firstCol = -1;
                        } else {
                            return;
                        }
                    } else if (firstCol == col) {
                        if (firstRow < row) {
                            int shipSize = selectedShip->text().toInt();
                            success = viewModel->placeShipSouth(new Ship(shipSize), firstRow, firstCol, true);
                            firstRow = -1;
                            firstCol = -1;
                        } else if (firstRow > row) {
                            int shipSize = selectedShip->text().toInt();
                            success = viewModel->placeShipNorth(new Ship(shipSize), firstRow, firstCol, true);
                            firstRow = -1;
                            firstCol = -1;
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }


                    if (success) {
                        selectedShip->setEnabled(false);
                        selectedShip->setStyleSheet("background-color: #DDDDDD;");
                        selectedShip = nullptr;
                        drawBoard();
                        shipsPlaced++;
                        if (shipsPlaced >= 5)
                            viewModel->setGameStarted(true);
                    }
                }
            });
        }
    }

    // select ship in dock to place
    for (int iship = 0; iship < 5; iship++) {
        QLayoutItem *shipLayoutItem = ui->docks->itemAt(iship);
        QWidget *ship = dynamic_cast<QWidgetItem*>(shipLayoutItem)->widget();
        QPushButton *button = (QPushButton*)ship;
        connect(button, &QAbstractButton::clicked, this, [=] {
            if (!selectedShip) { // select
                selectedShip = button;
                selectedShip->setStyleSheet("background-color: cyan;");
            } else {
                if (selectedShip->objectName() != button->objectName()) {
                    // deselect
                    selectedShip->setStyleSheet("background-color: #DDDDDD;");
                    selectedShip = nullptr;
                    firstRow = -1;
                    firstCol = -1;
                    // select
                    selectedShip = button;
                    selectedShip->setStyleSheet("background-color: cyan;");
                } else { // deselect
                    selectedShip->setStyleSheet("background-color: #DDDDDD;");
                    selectedShip = nullptr;
                    firstRow = -1;
                    firstCol = -1;
                }
            }
        });
    }

    drawBoard();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::drawBoard() {
    Cell ***topBoard = viewModel->getTopScreen();
    Cell ***bottomBoard = viewModel->getBottomScreen();

    for (int row = 0; row < DIMENSIONS; row++) {
        for (int col = 0; col < DIMENSIONS; col++) {
            QLayoutItem *topLayoutItem = ui->topScreen->itemAtPosition(row, col);
            QLayoutItem *bottomLayoutItem = ui->bottomScreen->itemAtPosition(row, col);
            QWidget *topCell = dynamic_cast<QWidgetItem*>(topLayoutItem)->widget();
            QWidget *bottomCell = dynamic_cast<QWidgetItem*>(bottomLayoutItem)->widget();
            if (false && topBoard[row][col]->hasShip()) {
                if (topBoard[row][col]->getPin() == RED)
                    topCell->setStyleSheet("background-color: red;");
                else if (topBoard[row][col]->getPin() == WHITE)
                    topCell->setStyleSheet("background-color: white;");
                else
                    topCell->setStyleSheet("background-color: gray;");
            } else {
                topCell->setStyleSheet("background-color: cyan;");
            }
            if (bottomBoard[row][col]->hasShip()) {
                if (bottomBoard[row][col]->getPin() == RED)
                    bottomCell->setStyleSheet("background-color: red;");
                else if (bottomBoard[row][col]->getPin() == WHITE)
                    bottomCell->setStyleSheet("background-color: white;");
                else
                    bottomCell->setStyleSheet("background-color: gray;");
            } else {
                bottomCell->setStyleSheet("background-color: cyan;");
            }
        }
    }
}

void MainWindow::reset() {
    viewModel->clearBoard();
    //viewModel->randomizeBoard(true);
    viewModel->randomizeBoard(false);
    ui->pShipsSunk->setText("Player ship sunk: 0");
    ui->oShipsSunk->setText("Opponent ship sunk: 0");
    drawBoard();
    shipsPlaced = 0;
    viewModel->setGameStarted(false);
    firstRow = -1;
    firstCol = -1;

    // enable docked ship buttons
    for (int iship = 0; iship < 5; iship++) {
        QLayoutItem *shipLayoutItem = ui->docks->itemAt(iship);
        QWidget *ship = dynamic_cast<QWidgetItem*>(shipLayoutItem)->widget();
        QPushButton *button = (QPushButton*)ship;
        button->setEnabled(true);
    }
}


