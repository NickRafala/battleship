#ifndef CELL_H
#define CELL_H

#include <QObject>
#include "ship.h"

enum PinColor {
    NONE,
    WHITE,
    RED
};

class Cell : public QObject
{
    Q_OBJECT
private:
    PinColor pin;
    bool containsShip;
    Ship *ship;
public:
    Cell();
    ~Cell();
    void setPin(PinColor _pin);
    PinColor getPin();
    void hasShip(bool has, Ship *_ship);
    bool hasShip();
    Ship* getShip();
signals:
    void pinPlaced(PinColor pin);
};

#endif // CELL_H
