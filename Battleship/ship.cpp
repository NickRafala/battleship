#include "ship.h"

Ship::Ship(int size)
    : numHoles(size)
    , numHits(0)
{

}

Ship::~Ship() {

}

void Ship::hit() {
    numHits++;
    if (numHits >= numHoles) {
        sunk = true;
        emit shipSunk();
    }
}

int Ship::getSize() {
    return numHoles;
}

void Ship::setSunk(bool _sunk) {
    sunk = _sunk;
}

bool Ship::getSunk() {
    return sunk;
}
