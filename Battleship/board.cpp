#include "board.h"

Board::Board()
    : shipsSunk(0)
{
    screen = new Cell**[DIMENSIONS];
    for (int i = 0; i < DIMENSIONS; i++) {
        screen[i] = new Cell*[DIMENSIONS];
        for (int ii = 0; ii < DIMENSIONS; ii++) {
            screen[i][ii] = new Cell();
            connect(screen[i][ii], &Cell::pinPlaced, this, [=](PinColor pin) {
                emit pinPlaced(pin, i, ii);
            });
        }
    }
}

Board::~Board() {
    delete ships[0];
    delete ships[1];
    delete ships[2];
    delete ships[3];
    delete ships[4];

    for (int i = 0; i < DIMENSIONS; i++) {
        for (int ii = 0; ii < DIMENSIONS; ii++) {
            delete screen[i][ii];
        }
        delete screen[i];
    }
    delete screen;
}

void Board::takeAttack(int row, int col, bool hit) {
    if (screen[row][col]->getPin() != NONE) return;
    screen[row][col]->setPin(hit ? RED : WHITE);
    if (screen[row][col]->hasShip()) screen[row][col]->getShip()->hit();
}

bool Board::hasShipAt(int row, int col) {
    return screen[row][col]->hasShip();
}

void Board::placeShipNorth(Ship *ship, int row, int col) {
    for (int i = row; i > (row - ship->getSize()); i--) {
        screen[i][col]->hasShip(true, ship);
    }
    ships.push_back(ship);
    connect(ship, &Ship::shipSunk, this, [=] {
        shipsSunk++;
        emit shipSunk(shipsSunk);
    });
}

void Board::placeShipSouth(Ship *ship, int row, int col) {
    for (int i = row; i < (row + ship->getSize()); i++) {
        screen[i][col]->hasShip(true, ship);
    }
    ships.push_back(ship);
    connect(ship, &Ship::shipSunk, this, [=] {
        shipsSunk++;
        emit shipSunk(shipsSunk);
    });
}

void Board::placeShipEast(Ship *ship, int row, int col) {
    for (int i = col; i < (col + ship->getSize()); i++) {
        screen[row][i]->hasShip(true, ship);
    }
    ships.push_back(ship);
    connect(ship, &Ship::shipSunk, this, [=] {
        shipsSunk++;
        emit shipSunk(shipsSunk);
    });
}

void Board::placeShipWest(Ship *ship, int row, int col) {
    for (int i = col; i > (col - ship->getSize()); i--) {
        screen[row][i]->hasShip(true, ship);
    }
    ships.push_back(ship);
    connect(ship, &Ship::shipSunk, this, [=] {
        shipsSunk++;
        emit shipSunk(shipsSunk);
    });
}

int Board::getShipsSunk() {
    return shipsSunk;
}

Cell*** Board::getScreen() {
    return screen;
}


