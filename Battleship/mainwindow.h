#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include "gameviewmodel.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void drawBoard();
    void reset();

private:
    Ui::MainWindow *ui;
    GameViewModel *viewModel;
    QPushButton *selectedShip;
    int shipsPlaced;
    int firstRow;
    int firstCol;

};
#endif // MAINWINDOW_H
