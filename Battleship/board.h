#ifndef BOARD_H
#define BOARD_H

#include <QObject>
#include <QVector>
#include "ship.h"
#include "cell.h"

#define DIMENSIONS 10

class Board : public QObject
{
    Q_OBJECT
private:
    Cell*** screen; // track your attacks
    QVector<Ship*> ships;
    int shipsSunk;
public:
    Board();
    ~Board();
    void takeAttack(int row, int col, bool hit);
    bool hasShipAt(int row, int col);
    void placeShipNorth(Ship *ship, int row, int col);
    void placeShipSouth(Ship *ship, int row, int col);
    void placeShipEast(Ship *ship, int row, int col);
    void placeShipWest(Ship *ship, int row, int col);

    int getShipsSunk();
    Cell*** getScreen();
signals:
    void gameOver();
    void pinPlaced(PinColor pin, int row, int col);
    void shipSunk(int sunk);
};

#endif // BOARD_H
