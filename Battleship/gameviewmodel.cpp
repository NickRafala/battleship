#include "gameviewmodel.h"

GameViewModel::GameViewModel()
    : playerBoard(new Board())
    , opponentBoard(new Board())
{
    connect(opponentBoard, &Board::pinPlaced, this, &GameViewModel::topPinPlaced);
    connect(opponentBoard, &Board::shipSunk, this, [=](int sunk) {
        emit shipSunk(false, sunk);
    });
    connect(playerBoard, &Board::pinPlaced, this, &GameViewModel::bottomPinPlaced);
    connect(playerBoard, &Board::shipSunk, this, [=](int sunk) {
        emit shipSunk(true, sunk);
    });
}

void GameViewModel::attack(int row, int col) {
    if (!gameStarted) return;
    // Start turn
    // Assume the player goes first

    // player turn
    if (opponentBoard->getScreen()[row][col]->getPin() != NONE) return;
    bool hit = opponentBoard->hasShipAt(row, col);
    opponentBoard->takeAttack(row, col, hit);
    if (opponentBoard->getShipsSunk() >= 5) {
        emit gameWin();
        return;
    }


    // opponent turn
    srand(time(NULL));
    do {
        int r = rand() % DIMENSIONS;
        int c = rand() % DIMENSIONS;
        if (playerBoard->getScreen()[r][c]->getPin() != NONE) continue;
        bool hit = playerBoard->hasShipAt(r, c);
        playerBoard->takeAttack(r, c, hit);
        if (playerBoard->getShipsSunk() >= 5) {
            emit gameLose();
        }
        break;
    } while(true);
}

bool GameViewModel::placeShipNorth(Ship *ship, int row, int col, bool player) {
    if (row - ship->getSize() < 0) return false;
    Board *board = nullptr;
    if (player) board = playerBoard;
    else board = opponentBoard;
    for (int i = row; i > row - ship->getSize(); i--) {
        if (board->getScreen()[i][col]->hasShip()) return false;
    }
    board->placeShipNorth(ship, row, col);
    return true;
}

bool GameViewModel::placeShipSouth(Ship *ship, int row, int col, bool player) {
    if (row + ship->getSize() > DIMENSIONS) return false;
    Board *board = nullptr;
    if (player) board = playerBoard;
    else board = opponentBoard;
    for (int i = row; i < row + ship->getSize(); i++) {
        if (board->getScreen()[i][col]->hasShip()) return false;
    }
    board->placeShipSouth(ship, row, col);
    return true;
}

bool GameViewModel::placeShipEast(Ship *ship, int row, int col, bool player) {
    if (col + ship->getSize() > DIMENSIONS) return false;
    Board *board = nullptr;
    if (player) board = playerBoard;
    else board = opponentBoard;
    for (int i = col; i < col + ship->getSize(); i++) {
        if (board->getScreen()[row][i]->hasShip()) return false;
    }
    board->placeShipEast(ship, row, col);
    return true;
}

bool GameViewModel::placeShipWest(Ship *ship, int row, int col, bool player) {
    if (col - ship->getSize() < 0) return false;
    Board *board = nullptr;
    if (player) board = playerBoard;
    else board = opponentBoard;
    for (int i = col; i > col - ship->getSize(); i--) {
        if (board->getScreen()[row][i]->hasShip()) return false;
    }
    board->placeShipWest(ship, row, col);
    return true;
}

void GameViewModel::clearBoard() {
    delete playerBoard;
    delete opponentBoard;
    playerBoard = new Board();
    opponentBoard = new Board();
    connect(opponentBoard, &Board::pinPlaced, this, &GameViewModel::topPinPlaced);
    connect(opponentBoard, &Board::shipSunk, this, [=](int sunk) {
        emit shipSunk(false, sunk);
    });
    connect(playerBoard, &Board::pinPlaced, this, &GameViewModel::bottomPinPlaced);
    connect(playerBoard, &Board::shipSunk, this, [=](int sunk) {
        emit shipSunk(true, sunk);
    });
}

void GameViewModel::randomizeBoard(bool playerBoard) {
    int sizes[] = {2, 3, 3, 4, 5};
    srand(time(NULL));
    for (int i = 0; i < 5; i++) {
        int direction = rand() % 4;
        Ship *ship = new Ship(sizes[i]);
        switch (direction) {
        case 0:
            while (!placeShipEast(ship, rand() % DIMENSIONS, rand() % DIMENSIONS, playerBoard)) {}
            break;
        case 1:
            while (!placeShipWest(ship, rand() % DIMENSIONS, rand() % DIMENSIONS, playerBoard)) {}
            break;
        case 2:
            while (!placeShipNorth(ship, rand() % DIMENSIONS, rand() % DIMENSIONS, playerBoard)) {}
            break;
        case 3:
            while (!placeShipSouth(ship, rand() % DIMENSIONS, rand() % DIMENSIONS, playerBoard)) {}
            break;
        }
    }
}

Cell*** GameViewModel::getTopScreen() {
    return opponentBoard->getScreen();
}

Cell*** GameViewModel::getBottomScreen() {
    return playerBoard->getScreen();
}

bool GameViewModel::getGameStarted() {
    return gameStarted;
}

void GameViewModel::setGameStarted(bool start) {
    gameStarted = start;
}


