#ifndef GAMEVIEWMODEL_H
#define GAMEVIEWMODEL_H

#include <QObject>
#include <QDebug>
#include "board.h"
#include "ship.h"

class GameViewModel : public QObject
{
    Q_OBJECT
private:
    Board *playerBoard;
    Board *opponentBoard;
    bool gameStarted;

public:
    GameViewModel();
    void attack(int row, int col);
    bool placeShipNorth(Ship *ship, int row, int col, bool player);
    bool placeShipSouth(Ship *ship, int row, int col, bool player);
    bool placeShipEast(Ship *ship, int row, int col, bool player);
    bool placeShipWest(Ship *ship, int row, int col, bool player);
    void clearBoard();
    void randomizeBoard(bool playerBoard);
    Cell*** getTopScreen();
    Cell*** getBottomScreen();
    bool getGameStarted();
    void setGameStarted(bool start);

signals:
    void gameWin();
    void gameLose();
    void topPinPlaced(PinColor pin, int row, int col);
    void bottomPinPlaced(PinColor pin, int row, int col);
    void shipSunk(bool isPlayer, int numSunk);
};

#endif // GAMEVIEWMODEL_H
