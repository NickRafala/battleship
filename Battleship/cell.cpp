#include "cell.h"

Cell::Cell()
    : pin(NONE)
    , containsShip(false)
{

}

Cell::~Cell()
{

}

void Cell::setPin(PinColor _pin) {
    pin = _pin;
    emit pinPlaced(pin);
}

PinColor Cell::getPin() {
    return pin;
}

void Cell::hasShip(bool has, Ship *_ship) {
    containsShip = has;
    ship = _ship;
}

bool Cell::hasShip() {
    return containsShip;
}

Ship* Cell::getShip() {
    return ship;
}
